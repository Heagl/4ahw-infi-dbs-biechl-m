package com.at.htlinn.ormm;
/*
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
*/
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class MyORMmanager {

	//private final static String DATABASE_URL = "jdbc:sqlite:D:/Schule/4aHWII/INFI-DBS/DumpTruck.db";
	private final static String DATABASE_URL = "jdbc:mysql://localhost:3306/dumptruckmanager?useSSL=false";

	private Dao<DumpTruck, Integer> dumpTruckDao;
	private Dao<DumpBin, Integer> dumpBinDao;
	private Dao<BinToTruck, Integer> dumpBinToTruckDao;

	public static void main(String[] args) throws Exception {
		new MyORMmanager().doMain(args);
	}

	private void doMain(String[] args) throws Exception {
		ConnectionSource connectionSource = null;
		try {
			// create our data-source for the database
			//connectionSource = new JdbcConnectionSource(DATABASE_URL);
			connectionSource = new JdbcConnectionSource(DATABASE_URL, "schule", "schule");
			// setup our database and DAOs
			setupDatabase(connectionSource);
			// read and write some data
			readWriteData();
			// do a bunch of bulk operations
		} finally {
			// destroy the data source which should close underlying connections
			if (connectionSource != null) {
				connectionSource.close();
			}
		}
	}

	/**
	 * Setup our database and DAOs
	 */
	private void setupDatabase(ConnectionSource connectionSource) throws Exception {

		dumpTruckDao = DaoManager.createDao(connectionSource, DumpTruck.class);
		dumpBinDao = DaoManager.createDao(connectionSource, DumpBin.class);
		dumpBinToTruckDao = DaoManager.createDao(connectionSource, BinToTruck.class);

		// if you need to create the table		
		TableUtils.dropTable(connectionSource, DumpTruck.class, false);
		TableUtils.dropTable(connectionSource, DumpBin.class, false);
		TableUtils.dropTable(connectionSource, BinToTruck.class, false);
		
		TableUtils.createTable(connectionSource, DumpTruck.class);
		TableUtils.createTable(connectionSource, DumpBin.class);
		TableUtils.createTable(connectionSource, BinToTruck.class);
	}
	
	private void readWriteData() throws Exception {
		// create an instance of DumpTruck
		
		DumpTruck dt1 = new DumpTruck("IL-333 333", "Scania 2700", 12000);
		DumpTruck dt2 = new DumpTruck("IL-333 334", "Scania 2700", 12000);
		DumpTruck dt3 = new DumpTruck("IL-333 335", "Scania 2700", 12000);
		DumpTruck dt4 = new DumpTruck("IL-333 336", "Scania 2700", 12000);
		
		DumpBin db1 = new DumpBin(1,2,"BinSter");
		DumpBin db2 = new DumpBin(2,4,"BinBois");
		
		BinToTruck btd1 = new BinToTruck(1,1);
		
		
		// persist the account object to the database
		
		dumpBinDao.create(db1);
		dumpTruckDao.create(dt1);
		dumpBinToTruckDao.create(btd1);
	}
}
