package com.at.htlinn.ormm;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "DumpBin")
public class DumpBin {
	
	@DatabaseField(id = true)
	private int id;	
	
	@DatabaseField(canBeNull = false)
	private int capacity;
	
	@DatabaseField(canBeNull = false)
	private String producer;
	
	public DumpBin(int id, int capacity, String producer) {
		super();
		this.id = id;
		this.capacity = capacity;
		this.producer = producer;
	}

	public DumpBin(){}
	
	public int getCapacity() {
		return capacity;
	}	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}
	
}
