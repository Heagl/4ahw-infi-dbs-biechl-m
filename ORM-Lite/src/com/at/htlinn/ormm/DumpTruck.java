package com.at.htlinn.ormm;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "DumpTruck")
public class DumpTruck {

	
	@DatabaseField(id = true)
	private String licenceplate;
	
	@DatabaseField(canBeNull = false)
	private String lkwType;
	
	@DatabaseField(canBeNull = false)
	private int capacity;
	
	/*
	public static void main(String[] args) {

	}
	*/
	
	public DumpTruck(){}  //Default Constructor

	public DumpTruck(String licenceplate, String lkwType, int capacity) { //Customized Constructor
		super();
		this.licenceplate = licenceplate;
		this.lkwType = lkwType;
		this.capacity = capacity;
	}

	public String getLicenceplate() {
		return licenceplate;
	}

	public void setLicenceplate(String licenceplate) {
		this.licenceplate = licenceplate;
	}

	public String getLkwType() {
		return lkwType;
	}

	public void setLkwType(String lkwType) {
		this.lkwType = lkwType;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
}
