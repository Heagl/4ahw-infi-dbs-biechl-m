import java.util.List;
import redis.clients.jedis.Jedis; 

public class Main {

	  public static void main(String[] args) { 
	      //Connecting to Redis server on localhost 
	      Jedis jedis = new Jedis("localhost"); 
	      jedis.connect();
	      System.out.println("Connection to server sucessfully"); 
	      System.out.println("Ping: "+jedis.ping());
	      jedis.del("Trashcan");
	      jedis.del("Dumptruck");
	      //store data in redis list 
	      jedis.lpush("Trashcan", "Trashcan Super", "Zull", "100 Liter"); 
	      jedis.lpush("Trashcan", "Trashcan Mega", "Traptrap", "50 Liter");
	      jedis.lpush("Trashcan", "Trashcan Small", "Jaegermasta", "5 Liter");
	      jedis.lpush("Dumptruck", "Volvo", "ABCD", "IL 2456"); 
	      jedis.lpush("Dumptruck", "Scania", "FK-U", "IL 9821"); 
	      // Get the stored data and print it 
	      List<String> trashcans = jedis.lrange("Trashcan", 0 ,-1); 
	      List<String> dumptruck = jedis.lrange("Dumptruck", 0 ,-1); 
	      
	      for(int i = trashcans.size(); i > 0; i= i-3) { 
	         System.out.println("Stored string in redis: "+trashcans.get(i-1)+ ", "+ trashcans.get(i-2)+", "+ trashcans.get(i-3)); 
	      }
	      
	      for(int i = dumptruck.size(); i > 0;  i= i-3) { 
		         System.out.println("Stored string in redis: "+dumptruck.get(i-1)+ ", "+ dumptruck.get(i-2)+ ", "+ dumptruck.get(i-3)); 
		      }
	      
	  }
}
