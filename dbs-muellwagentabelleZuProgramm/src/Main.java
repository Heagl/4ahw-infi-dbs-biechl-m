import java.awt.List;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class Main {

	static Scanner sc = new Scanner(System.in);
	private static String user = "";
	private static String password = "";
	private static Connection c;

	public static void main(String[] args) {

		try
		{

			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Sql User:");
			user = sc.nextLine();
			password = sc.nextLine();

			c = DriverManager.getConnection("jdbc:mysql://localhost:3306", user, password);
			Statement stmt =  c.createStatement();
			String sql = "Create Database IF NOT EXISTS  Mülllaster_Stadteinteilung";
			stmt.executeUpdate(sql);
			stmt.executeUpdate("USE Mülllaster_Stadteinteilung");
			stmt.close();
			createTablesPremade();
			consoleInput();
			c.close();

		}
		catch(Exception e)
		{
			System.err.println(e.getClass().getName()+": "+ e.getMessage());
			System.exit(0);
		}
	}

	public static void consoleInput() throws SQLException, IOException
	{
		String console;
		System.out.println("Commands:");
		System.out.println("END to exit the Programm");
		System.out.println("INSERT to insert data via a file path");
		System.out.println("SELECT to select data from tables");

		while(true)
		{
			console = sc.nextLine();

			if(console.equals("END"))
			{
				break;
			}

			if(console.equals("SELECT"))
			{
				System.out.println("put Select statement here:");
				console = sc.nextLine();
				selectResult(console);
			}
			
			if(console.equals("INSERT"))
			{
				System.out.println("In what table do you want to add the file?");
				console = sc.nextLine();
				if(console.equals("mülllaster_Hersteller"))
				{
					System.out.println("Insert Files path?");
					sc.nextLine();
					insertIntoTable("mülllaster_Hersteller", console);
				}

				if(console.equals("mülllaster"))
				{
					System.out.println("Insert Files path?");
					sc.nextLine();
					insertIntoTable("mülllaster", console);
				}

				if(console.equals("mülltonnen_Hersteller"))
				{
					System.out.println("Insert Files path?");
					sc.nextLine();
					insertIntoTable("mülltonnen_Hersteller", console);
				}

				if(console.equals("mülltonnen"))
				{
					System.out.println("Insert Files path?");
					sc.nextLine();
					insertIntoTable("mülltonnen", console);
				}

				if(console.equals("Fahrteneinteilung"))
				{
					System.out.println("Insert Files path?");
					sc.nextLine();
					insertIntoTable("Fahrteneinteilung", console);
				}

				if(console.equals("Stadtberreiche"))
				{
					System.out.println("Insert Files path?");
					sc.nextLine();
					insertIntoTable("Stadtberreiche", console);
				}

				if(console.equals("ZugewieseneContainerTypen"))
				{
					System.out.println("Insert Files path?");
					sc.nextLine();
					insertIntoTable("ZugewieseneContainerTypen", console);
				}
			}

		}
	}

	public static void selectResult(String sql1) throws SQLException
	{
		ArrayList<String> selectStatement = (ArrayList<String>) Arrays.asList(sql1.split(","));	
		String[] tableRows = null;
		String results = "";
		Statement stmt = c.createStatement();
		ResultSet rs;
		rs = stmt.executeQuery("SHOW Colums FROM"+ selectStatement.get(1));
		int j = 0;
		while(rs.next())
		{
			tableRows[j] = rs.getString(j);
			j++;
		}
		rs = stmt.executeQuery(sql1);
		while(rs.next())
		{
			for(int i = 0; i <= tableRows.length; i++)
			results = results + rs.getString(i);
			results = results + " / ";
		}
		stmt.close();
	}

	public static void createTablesPremade() throws SQLException
	{
		Statement stmt = c.createStatement();
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Mülllaster_Stadteinteilung.Stadtberreiche (id_Stadtberreiche TINYINT NOT NULL,name_Stadtberreich VARCHAR(45) NULL,PRIMARY KEY (id_Stadtberreiche))ENGINE = InnoDB;");
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Mülllaster_Stadteinteilung.mülltonnen_Hersteller (id_mülltonnen_Hersteller INT NOT NULL AUTO_INCREMENT, Hersteller_name VARCHAR(45) NULL, Hersteller_addresse VARCHAR(45) NULL, Hersteller_ansprechperson VARCHAR(120) NULL,PRIMARY KEY (id_mülltonnen_Hersteller))ENGINE = InnoDB;");
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Mülllaster_Stadteinteilung.mülllaster_Hersteller (id_mülllaster_Hersteller TINYINT NOT NULL AUTO_INCREMENT, Hersteller_name VARCHAR(45) NULL,Hersteller_addresse VARCHAR(45) NULL, Hersteller_ansprechpartner VARCHAR(45) NULL, PRIMARY KEY (id_mülllaster_Hersteller), CONSTRAINT Mülllaster_ID FOREIGN KEY (id_mülllaster_Hersteller) REFERENCES Mülllaster_Stadteinteilung.mülllaster (Hersteller_ID) ON DELETE NO ACTION ON UPDATE NO ACTION) ENGINE = InnoDB;");
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Mülllaster_Stadteinteilung.mülllaster (idmülllaster TINYINT NOT NULL AUTO_INCREMENT,Kennzeichen VARCHAR(45) NULL,Hersteller_ID TINYINT NOT NULL,model_Nr VARCHAR(45) NULL,PRIMARY KEY (idmülllaster, Hersteller_ID, Kennzeichen),CONSTRAINT Fahrteneinteilung FOREIGN KEY () REFERENCES Mülllaster_Stadteinteilung.Fahrteneinteilung() ON DELETE NO ACTION ON UPDATE NO ACTION)ENGINE = InnoDB;");
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Mülllaster_Stadteinteilung.mülltonnen (id_mülltonnen INT NOT NULL AUTO_INCREMENT,hersteller_id INT NULL,modelname VARCHAR(45) NULL,fassungsvermögen INT NULL,stadtbereich_id TINYINT NULL,PRIMARY KEY (id_mülltonnen) INDEX Hersteller_ID_idx (hersteller_id ASC),INDEX Stadtbereich_ID_idx (stadtbereich_id ASC),CONSTRAINT Hersteller_ID FOREIGN KEY (hersteller_id)REFERENCES Mülllaster_Stadteinteilung.mülltonnen_Hersteller (id_mülltonnen_Hersteller) ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT Stadtbereich_ID FOREIGN KEY (stadtbereich_id) REFERENCES Mülllaster_Stadteinteilung.Stadtberreiche (id_Stadtberreiche) ON DELETE NO ACTION ON UPDATE NO ACTION)ENGINE = InnoDB;");
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Mülllaster_Stadteinteilung.ZugewieseneContainerTypen (mülllaster_id TINYINT NOT NULL, müllcontainter_id INT NOT NULL, PRIMARY KEY (mülllaster_id, müllcontainter_id), INDEX mülltonnen_id_idx (müllcontainter_id ASC), CONSTRAINT mülllaster_id FOREIGN KEY (mülllaster_id) REFERENCES Mülllaster_Stadteinteilung.mülllaster (idmülllaster) ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT mülltonnen_id FOREIGN KEY (müllcontainter_id) REFERENCES Mülllaster_Stadteinteilung.mülltonnen (id_mülltonnen) ON DELETE NO ACTION ON UPDATE NO ACTION) ENGINE = InnoDB;");
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Mülllaster_Stadteinteilung.Fahrteneinteilung (id_Fahrteneinteilung INT NOT NULL AUTO_INCREMENT,Tag VARCHAR(3) NULL,Uhrzeit TIME NULL,Müllwagen_ID TINYINT NOT NULL, Stadtberreich_ID TINYINT NOT NULL, PRIMARY KEY (id_Fahrteneinteilung, Müllwagen_ID, Stadtberreich_ID), INDEX Müllwagen ID_idx (Müllwagen_ID ASC), INDEX Stadtberreich ID_idx (Stadtberreich_ID ASC), CONSTRAINT Müllwagen ID FOREIGN KEY (Müllwagen_ID) REFERENCES Mülllaster_Stadteinteilung.mülllaster (idmülllaster) ON DELETE NO ACTION ON UPDATE NO ACTION CONSTRAINT Stadtberreich ID FOREIGN KEY (Stadtberreich_ID)REFERENCES Mülllaster_Stadteinteilung.Stadtberreiche (id_Stadtberreiche)ON DELETE NO ACTION ON UPDATE NO ACTION)ENGINE = InnoDB;");

		stmt.close();

		System.out.println("Tables Created Succesfully");
	}

	public static void  insertIntoTable(String table, String filePath) throws SQLException, IOException
	{
		String values = "";
		BufferedReader file = new BufferedReader(new FileReader(filePath));
		String sql = file.readLine();
		String[] valuesArray = sql.split("\\s*,\\s*");		
		Statement stmt = c.createStatement();
		for(int i = 0; i < valuesArray.length; i++)
		{
			values = values + valuesArray[i];
			values = values + ", ";
		}
		stmt.executeUpdate("INSERT INTO "+table+" values ("+ values+");");
		stmt.close();
		file.close();
	}
}