package com.at.htlinn.ormm;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "DumpBinToDumpTruck")
public class BinToTruck {
	
	@DatabaseField(id = true)
	private int dumpTruckId;
	
	@DatabaseField(canBeNull = false )
	private int DumpBinId;

	public BinToTruck(){}
	
	public BinToTruck(int dumpTruckId, int dumpBinId) {
		super();
		this.dumpTruckId = dumpTruckId;
		DumpBinId = dumpBinId;
	}

	public int getDumpTruckId() {
		return dumpTruckId;
	}

	public void setDumpTruckId(int dumpTruckId) {
		this.dumpTruckId = dumpTruckId;
	}

	public int getDumpBinId() {
		return DumpBinId;
	}

	public void setDumpBinId(int dumpBinId) {
		DumpBinId = dumpBinId;
	}
	
	
	
	
}
